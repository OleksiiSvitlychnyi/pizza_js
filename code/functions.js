import { pizzaUser, pizzaBD } from "./data-pizza.js";

const table = document.querySelector(".table");

const pizzaSelectSize = (e) => {
    if(e.target.tagName === "INPUT" && e.target.checked){
        pizzaUser.size = pizzaBD.size.find(el=>el.name === e.target.id);
    }
    show(pizzaUser)
};

const pizzaSelectTopping = (e) => {
    switch (e.target.id) {
        case "sauceClassic" : pizzaUser.sauce = pizzaBD.sauce.find(el => el.name === e.target.id);
        break;
        case "sauceBBQ" : pizzaUser.sauce = pizzaBD.sauce.find(el => el.name === e.target.id);
        break;
        case "sauceRikotta" : pizzaUser.sauce = pizzaBD.sauce.find(el => el.name === e.target.id);
        break;
        case "moc1" : pizzaUser.topping.push(pizzaBD.topping.find(el => el.name === e.target.id));
        break;
        case "moc2" : pizzaUser.topping.push(pizzaBD.topping.find(el => el.name === e.target.id));
        break;
        case "moc3" : pizzaUser.topping.push(pizzaBD.topping.find(el => el.name === e.target.id));
        break;
        case "telya" : pizzaUser.topping.push(pizzaBD.topping.find(el => el.name === e.target.id));
        break;
        case "vetch1" : pizzaUser.topping.push(pizzaBD.topping.find(el => el.name === e.target.id));
        break;
        case "vetch2" : pizzaUser.topping.push(pizzaBD.topping.find(el => el.name === e.target.id));
        break;
    }
    show(pizzaUser);
    if(e.target.tagName === "IMG"){
        //if ()
        table.childNodes.forEach((el)=>{
            if(el.className === "sauceClassic" && e.target.id !== "moc1" && e.target.id !== "moc2" && e.target.id !== "moc3" && e.target.id !== "telya" && e.target.id !== "vetch1" && e.target.id !== "vetch2"
             || el.className === "sauceBBQ" && e.target.id !== "moc1" && e.target.id !== "moc2" && e.target.id !== "moc3" && e.target.id !== "telya" && e.target.id !== "vetch1" && e.target.id !== "vetch2"
             || el.className === "sauceRikotta" && e.target.id !== "moc1" && e.target.id !== "moc2" && e.target.id !== "moc3" && e.target.id !== "telya" && e.target.id !== "vetch1" && e.target.id !== "vetch2"){
                document.querySelector(`.${el.className}`).remove();
            }
            else if(el.className === "moc1" && e.target.id === "moc1" || el.className === "moc2" && e.target.id === "moc2" || el.className === "moc3" && e.target.id === "moc3" || el.className === "telya" && e.target.id === "telya" || el.className === "vetch1" && e.target.id === "vetch1" || el.className === "vetch2" && e.target.id === "vetch2"){
                document.querySelector(`.${el.className}`).remove();
            }
        });

        table.insertAdjacentHTML("beforeend",`<img src="${e.target.src}" class="${e.target.id}" alt="${e.target.alt}">`);
    }
};

const pizzaSelectToppingDrag = (e) => {
    switch (e.id) {
        case "sauceClassic" : pizzaUser.sauce = pizzaBD.sauce.find(el => el.name === e.id);
        break;
        case "sauceBBQ" : pizzaUser.sauce = pizzaBD.sauce.find(el => el.name === e.id);
        break;
        case "sauceRikotta" : pizzaUser.sauce = pizzaBD.sauce.find(el => el.name === e.id);
        break;
        case "moc1" : pizzaUser.topping.push(pizzaBD.topping.find(el => el.name === e.id));
        break;
        case "moc2" : pizzaUser.topping.push(pizzaBD.topping.find(el => el.name === e.id));
        break;
        case "moc3" : pizzaUser.topping.push(pizzaBD.topping.find(el => el.name === e.id));
        break;
        case "telya" : pizzaUser.topping.push(pizzaBD.topping.find(el => el.name === e.id));
        break;
        case "vetch1" : pizzaUser.topping.push(pizzaBD.topping.find(el => el.name === e.id));
        break;
        case "vetch2" : pizzaUser.topping.push(pizzaBD.topping.find(el => el.name === e.id));
        break;
    }
    show(pizzaUser);
    if(e.tagName === "IMG"){
        table.childNodes.forEach((el)=>{
            if(el.className === "sauceClassic" && e.id !== "moc1" && e.id !== "moc2" && e.id !== "moc3" && e.id !== "telya" && e.id !== "vetch1" && e.id !== "vetch2"
             || el.className === "sauceBBQ" && e.id !== "moc1" && e.id !== "moc2" && e.id !== "moc3" && e.id !== "telya" && e.id !== "vetch1" && e.id !== "vetch2"
             || el.className === "sauceRikotta" && e.id !== "moc1" && e.id !== "moc2" && e.id !== "moc3" && e.id !== "telya" && e.id !== "vetch1" && e.id !== "vetch2"){
                document.querySelector(`.${el.className}`).remove();
            }
            else if(el.className === "moc1" && e.id === "moc1" || el.className === "moc2" && e.id === "moc2" || el.className === "moc3" && e.id === "moc3" || el.className === "telya" && e.id === "telya" || el.className === "vetch1" && e.id === "vetch1" || el.className === "vetch2" && e.id === "vetch2"){
                document.querySelector(`.${el.className}`).remove();  
            }
        });

        table.insertAdjacentHTML("beforeend",`<img src="${e.src}" class="${e.id}" alt="${e.alt}">`);
    }
}

function show (pizza) {
    const price = document.getElementById("price");
    const sauce = document.getElementById("sauce");
    

    let totalPrice = 0;
    if(pizza.size !== ""){
        totalPrice += parseFloat(pizza.size.price);
    }
    if(pizza.sauce !== ""){
        totalPrice += parseFloat(pizza.sauce.price);
    }
    if(pizza.topping.length){
        totalPrice += pizza.topping.reduce((a,b)=>a + b.price, 0)
    }
    price.innerText = totalPrice;
   
    if(pizza.sauce !== ""){
        sauce.innerHTML = `<span class="topping">${pizza.sauce.productName}</span>`
    }

    if(Array.isArray(pizza.topping)){

        pizzaShowTopping(pizzaUser);        

        const [...toppingBtns] = document.querySelectorAll(".topping__btn");

        toppingBtns.forEach((toppingBtn)=>{
            toppingBtn.addEventListener("click", (e)=>{            
                totalPrice = totalPrice - pizzaUser.topping.find(el => el.productName === e.target.parentElement.parentElement.textContent.replace(/[0-9]{1,}X/gi, '')).price; // Вычитание цены удаленного топпинга!

                price.innerText = totalPrice;

                pizzaUser.topping.splice(pizzaUser.topping.findIndex(el => el.productName === e.target.parentElement.parentElement.textContent.replace(/[0-9]{1,}X/gi, '')), 1); // Удаление топпинга                

                let topping__count = e.target.parentElement.parentElement.firstChild.firstChild; // Работа со счетчиком топпинга

                if (topping__count.innerHTML > 1) {
                    topping__count.innerHTML = topping__count.innerHTML - 1;
                } else {
                    table.childNodes.forEach((el)=>{
                        if(el.alt === e.target.parentElement.parentElement.textContent.replace(/[^a-zа-яё\s]X/gi, '')){
                            document.querySelector(`.${el.className}`).remove();        
                        }                        
                    });

                    e.target.parentElement.parentElement.remove();                    
                }
            });
        });

        //if (pizzaUser.topping.length) console.log(pizzaUser); // инфо о всех добавленных топпингах
    }

    pizzaUser.price = totalPrice;
    pizzaUser.data = new Date()
};

const pizzaShowTopping = (pizza) => {
    const topping = document.getElementById("topping");

    topping.innerHTML = "";
        
    let result = {};

    pizza.topping.forEach(function(a){
        result[a.productName] = result[a.productName] + 1 || 1;
    });
    for (let key in result){
        topping.insertAdjacentHTML("beforeend",`<span class="topping"><span class="topping__info"><span class="topping__count">${result[key]}</span><button class="topping__btn">X</button></span>${key}</span>`)            
    }
};

const validate = (pattern, value) => pattern.test(value);

const random = (min, max) => {
    const rand = min + Math.random() * (max - min + 1);
    return Math.floor(rand);
};

export {pizzaSelectSize, pizzaSelectTopping, show, validate, pizzaSelectToppingDrag, random};